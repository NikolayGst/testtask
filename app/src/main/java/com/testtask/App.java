package com.testtask;

import com.orm.SugarApp;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class App extends SugarApp {

  private static App instance;

  private Cicerone<Router> cicerone;

  @Override
  public void onCreate() {
    super.onCreate();

    instance = this;

    initCicerone();
  }


  private void initCicerone() {
    cicerone = Cicerone.create();
  }

  public static App getInstance() {
    return instance;
  }

  public NavigatorHolder getNavigatorHolder() {
    return cicerone.getNavigatorHolder();
  }

  public Router getRouter() {
    return cicerone.getRouter();
  }

}
