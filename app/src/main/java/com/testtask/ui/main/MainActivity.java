package com.testtask.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.testtask.App;
import com.testtask.R;
import com.testtask.ui.common.Screens;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;

public class MainActivity extends MvpAppCompatActivity {

  private SupportAppNavigator supportAppNavigator = new SupportAppNavigator(this, R.id.container) {
    @Override
    protected void setupFragmentTransaction(Command command, Fragment currentFragment,
        Fragment nextFragment, FragmentTransaction fragmentTransaction) {
      fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
      super.setupFragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction);
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    if (savedInstanceState == null) {
      App.getInstance().getRouter().newRootChain(new Screens.SpecialtyListScreen());
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    App.getInstance().getNavigatorHolder().setNavigator(supportAppNavigator);
  }

  @Override
  protected void onPause() {
    super.onPause();
    App.getInstance().getNavigatorHolder().removeNavigator();
  }

}
