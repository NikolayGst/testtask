package com.testtask.ui.splash;

import com.arellomobile.mvp.InjectViewState;
import com.testtask.services.DataService;
import com.testtask.ui.common.BasePresenter;

@InjectViewState
public class SplashPresenter extends BasePresenter<SplashView> {

  SplashPresenter() {

    DataService dataService = new DataService();

    track(dataService.getData()
        .subscribe(
            data -> getViewState().onSuccessLoadData(),
            throwable -> getViewState().onErrorLoadData(throwable)));
  }

}
