package com.testtask.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.testtask.R;
import com.testtask.ui.main.MainActivity;

public class SplashActivity extends MvpAppCompatActivity implements SplashView {

  @InjectPresenter
  SplashPresenter splashPresenter;

  private ImageView imageView;
  private ProgressBar progressBar;
  private TextView errorMsg;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    imageView = findViewById(R.id.imageView);
    progressBar = findViewById(R.id.progress);
    errorMsg = findViewById(R.id.error_msg);

  }

  @Override
  public void onSuccessLoadData() {
    startActivity(new Intent(this, MainActivity.class));
    finish();
  }

  @Override
  public void onErrorLoadData(Throwable throwable) {

    imageView.setImageResource(R.drawable.ic_error);
    progressBar.setVisibility(View.GONE);
    errorMsg.setVisibility(View.VISIBLE);
  }

}
