package com.testtask.ui.specialties;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.testtask.App;
import com.testtask.R;
import com.testtask.items.SpecialtyItem;
import com.testtask.model.Specialty;
import com.testtask.ui.common.BaseFragment;
import com.testtask.ui.common.Screens.UserListScreen;
import java.util.List;

public class SpecialtyListFragment extends BaseFragment {

  private FastAdapter<SpecialtyItem> adapter;
  private ItemAdapter<SpecialtyItem> itemAdapter;
  private RecyclerView recyclerView;

  public SpecialtyListFragment() {
  }


  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    setTitle(R.string.specialty_title);

    View view = inflater.inflate(R.layout.fragment_specialty_list, container, false);

    initRecycler(view);

    return view;
  }

  private void initRecycler(View view) {
    recyclerView = view.findViewById(R.id.recyclerView);
    recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

    itemAdapter = new ItemAdapter<>();
    adapter = FastAdapter.with(itemAdapter);

    recyclerView.setAdapter(adapter);

    List<Specialty> specialties = Specialty.listAll(Specialty.class);

    if (!specialties.isEmpty()) {
      for (Specialty specialty : specialties) {
        itemAdapter.add(new SpecialtyItem(specialty));
      }
    }

    adapter.withOnClickListener((v, adapter, item, position) -> {
      App.getInstance().getRouter().navigateTo(new UserListScreen(item.getSpecialty().getSpecialtyId()));
      return true;
    });
  }

}
