package com.testtask.ui.common;

import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import com.testtask.ui.main.MainActivity;

public class BaseFragment extends Fragment {

  public void setTitle(@StringRes int title) {
    setTitle(getString(title));
  }

  public void setTitle(String title) {
    if (getActivity() instanceof MainActivity) {
      MainActivity mainActivity = (MainActivity) getActivity();
      if (mainActivity.getSupportActionBar() != null) {
        mainActivity.getSupportActionBar().setTitle(title);
      }
    }
  }

}
