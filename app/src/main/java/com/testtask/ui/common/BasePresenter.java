package com.testtask.ui.common;

import android.util.Log;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<T extends MvpView> extends MvpPresenter<T> {

  private static String TAG = BasePresenter.class.getSimpleName();

  private CompositeDisposable compositeDisposable = new CompositeDisposable();

  /**
   * Метод для сохранения подписок в коллекцию
   */
  protected void track(Disposable disposable) {
    compositeDisposable.add(disposable);
  }

  /**
   * Если есть подписки, отписываем их
   */
  private void unsubscribeAll() {
    Log.d(TAG, "Отписываемся от подписок: " + compositeDisposable.size());
    if (!compositeDisposable.isDisposed()) {
      compositeDisposable.clear();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    unsubscribeAll();
  }

}
