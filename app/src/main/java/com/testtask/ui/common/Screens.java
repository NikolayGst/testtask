package com.testtask.ui.common;

import android.support.v4.app.Fragment;
import com.testtask.ui.specialties.SpecialtyListFragment;
import com.testtask.ui.users.UserDetailFragment;
import com.testtask.ui.users.UserListFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

/**
 * Фрагменты
 */
public class Screens {

  /**
   * Список специальностей
   */
  public static class SpecialtyListScreen extends SupportAppScreen {

    @Override
    public Fragment getFragment() {
      return new SpecialtyListFragment();
    }

  }

  /**
   * Список работников
   */
  public static class UserListScreen extends SupportAppScreen {

    private int specialtyId;

    public UserListScreen(Integer specialtyId) {
      this.specialtyId = specialtyId;
    }

    @Override
    public Fragment getFragment() {
      return UserListFragment.newInstance(specialtyId);
    }

  }

  /**
   * Детали работника
   */
  public static class UserDetailScreen extends SupportAppScreen {

    private long userId;

    public UserDetailScreen(long userId) {
      this.userId = userId;
    }

    @Override
    public Fragment getFragment() {
      return UserDetailFragment.newInstance(userId);
    }

  }

}
