package com.testtask.ui.users;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.testtask.R;
import com.testtask.model.User;
import com.testtask.ui.common.BaseFragment;
import com.testtask.utils.Constants;
import com.testtask.utils.TimeUtils;

public class UserDetailFragment extends BaseFragment {


  private TextView textUserName;
  private ImageView avatarView;
  private TextView birthdayView;
  private TextView specialtyView;

  private long userId = -1;

  public UserDetailFragment() {
  }

  public static UserDetailFragment newInstance(long userId) {
    Bundle args = new Bundle();
    UserDetailFragment fragment = new UserDetailFragment();
    args.putLong(Constants.USER_ID_KEY, userId);
    fragment.setArguments(args);
    return fragment;
  }


  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    if (getArguments() != null) {
      userId = getArguments().getLong(Constants.USER_ID_KEY);
    }

    View view = inflater.inflate(R.layout.fragment_user_detail, container, false);

    setTitle(R.string.detail_user_title);

    initView(view);

    setData();

    return view;
  }


  private void initView(View view) {
    textUserName = view.findViewById(R.id.textUserName);
    avatarView = view.findViewById(R.id.avatarView);
    birthdayView = view.findViewById(R.id.birthdayView);
    specialtyView = view.findViewById(R.id.specialtyView);
  }

  private void setData() {
    if (userId != -1) {
      //Получаем пользователя из БД
      User user = User.findById(User.class, userId);
      if (user != null) {

        //Установка имени и фамилии
        textUserName.setText(user.getFirstName() != null && user.getLastName() != null
            ? user.getFirstName() + " " + user.getLastName()
            : "-");


        //Загрузка аватарки
        if (user.getAvatarUrl() != null && !user.getAvatarUrl().isEmpty()) {
          Picasso.get()
              .load(user.getAvatarUrl())
              .placeholder(R.drawable.person)
              .error(R.drawable.person)
              .into(avatarView);
        }


        //установка даты рождения
        birthdayView.setText(TimeUtils.formatDateForDetail(user.getBirthday()));

        //установка специальности
        specialtyView.setText(user.getSpecialtyName());
      }
    }
  }

}
