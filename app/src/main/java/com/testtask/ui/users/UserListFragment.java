package com.testtask.ui.users;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.testtask.App;
import com.testtask.R;
import com.testtask.items.UserItem;
import com.testtask.model.User;
import com.testtask.ui.common.BaseFragment;
import com.testtask.ui.common.Screens.UserDetailScreen;
import com.testtask.utils.Constants;
import java.util.List;

public class UserListFragment extends BaseFragment {

  private FastAdapter<UserItem> adapter;
  private ItemAdapter<UserItem> itemAdapter;
  private RecyclerView recyclerView;

  private Integer specialtyId = -1;

  public UserListFragment() {
  }

  public static UserListFragment newInstance(Integer specialtyId) {
    Bundle args = new Bundle();
    UserListFragment fragment = new UserListFragment();
    args.putInt(Constants.SPECIALTY_ID_KEY, specialtyId);
    fragment.setArguments(args);
    return fragment;
  }


  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    if (getArguments() != null) {
      specialtyId = getArguments().getInt(Constants.SPECIALTY_ID_KEY);
    }

    setTitle(R.string.users_title);

    View view = inflater.inflate(R.layout.fragment_user_list, container, false);

    initRecycler(view);

    return view;
  }

  private void initRecycler(View view) {

    recyclerView = view.findViewById(R.id.recyclerView);
    recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

    itemAdapter = new ItemAdapter<>();
    adapter = FastAdapter.with(itemAdapter);

    recyclerView.setAdapter(adapter);

    List<User> users = User.find(User.class, "specialty_id = ?", Integer.toString(specialtyId));

    if (!users.isEmpty()) {
      for (User user : users) {
        itemAdapter.add(new UserItem(user));
      }
    }

    adapter.withOnClickListener((v, adapter, item, position) -> {
      App.getInstance().getRouter().navigateTo(new UserDetailScreen(item.getUser().getId()));
      return true;
    });
  }

}
