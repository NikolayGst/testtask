package com.testtask.services;

import com.testtask.utils.Constants;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.http.GET;

/**
* Класс для создания объекта для сетевого взаимодействия
* */
class ApiFactory {

  private static API api;

  /**
  * GET запрос для получения данных
  * */
  interface API {

    @GET("/65gb/static/raw/master/testTask.json")
    Observable<ResponseBody> getData();

  }

  static API getApi() {
    if (api == null) {
      Retrofit retrofit = new Retrofit.Builder()
          .baseUrl(Constants.BASE_URL)
          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
          .build();

      api = retrofit.create(API.class);
    }
    return api;
  }

}
