package com.testtask.services;

import com.testtask.model.Data;
import com.testtask.model.Specialty;
import com.testtask.model.User;
import com.testtask.utils.StringUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.List;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Класс для обработки запроса по получению списка специальностей и работников
 */
public class DataService {

  public Observable<Data> getData() {
    return ApiFactory.getApi().getData()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .map(this::parseData)
        .doOnNext(this::writeDataToDb);
  }

  private Data parseData(ResponseBody data) throws IOException, JSONException {

    JSONObject jsonObject = new JSONObject(data.string());

    JSONArray response = jsonObject.getJSONArray("response");

    //Получаем список пользователей
    List<User> users = Observable.range(0, response.length())
        .map(response::getJSONObject)
        .map(item -> {
          User user = new User();
          user.setFirstName(StringUtils.upperFirstLetter(item.getString("f_name")));
          user.setLastName(StringUtils.upperFirstLetter(item.getString("l_name")));
          user.setBirthday(item.getString("birthday"));
          user.setAvatarUrl(item.getString("avatr_url"));
          JSONObject specialty = item.getJSONArray("specialty").getJSONObject(0);
          user.setSpecialtyId(specialty.getInt("specialty_id"));
          user.setSpecialtyName(specialty.getString("name"));
          return user;
        }).toList().blockingGet();

    //Получаем список специальностей
    List<Specialty> specialties = Observable.range(0, response.length())
        .map(response::getJSONObject)
        .map(item -> {
          JSONObject jsonSpecialty = item.getJSONArray("specialty").getJSONObject(0);
          return new Specialty(jsonSpecialty.getInt("specialty_id"),
              jsonSpecialty.getString("name"));
        }) //Удаляем дубликаты специальностей
        .distinct(Specialty::getSpecialtyId)
        .toList().blockingGet();

    return new Data(specialties, users);
  }

  private void writeDataToDb(Data data) {
    //Очищаем БД
    Specialty.deleteAll(Specialty.class);
    User.deleteAll(User.class);

    //Записываем специальности в БД
    for (Specialty specialty : data.getSpecialties()) {
      specialty.save();
    }

    //Записываем пользователей в БД
    for (User user : data.getUsers()) {
      user.save();
    }

  }
}
