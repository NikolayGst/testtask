package com.testtask.items;

import static com.testtask.utils.Constants.USER_ITEM_ID;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.testtask.R;
import com.testtask.model.User;
import com.testtask.utils.StringUtils;
import com.testtask.utils.TimeUtils;
import java.util.List;

/**
 * Элемент для списка работников
 */
public class UserItem extends AbstractItem<UserItem, UserItem.ViewHolder> {

  private User user;

  public UserItem(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  @NonNull
  @Override
  public ViewHolder getViewHolder(View v) {
    return new ViewHolder(v);
  }

  @Override
  public int getType() {
    return USER_ITEM_ID;
  }

  @Override
  public int getLayoutRes() {
    return R.layout.user_item;
  }

  public class ViewHolder extends FastAdapter.ViewHolder<UserItem> {

    private TextView textUserName;
    private TextView textYear;

    ViewHolder(View itemView) {
      super(itemView);
      textUserName = itemView.findViewById(R.id.textUserName);
      textYear = itemView.findViewById(R.id.textYear);
    }

    @Override
    public void bindView(UserItem item, List<Object> payloads) {

      int year = TimeUtils.getYearByBirthday(item.getUser().getBirthday());

      textUserName.setText(item.getUser().getFirstName() != null && item.getUser().getLastName() != null
          ? item.getUser().getFirstName() + " " + item.getUser().getLastName()
          : "-");

      textYear.setText(year == -1
          ? "-"
          : year + " " + StringUtils.getNumEnding(year, "год", "года", "лет"));

    }

    @Override
    public void unbindView(UserItem item) {
      textUserName.setText(null);
      textYear.setText(null);
    }

  }
}
