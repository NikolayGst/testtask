package com.testtask.items;

import static com.testtask.utils.Constants.SPECIALTY_ITEM_ID;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.testtask.R;
import com.testtask.model.Specialty;
import java.util.List;

/**
 * Элемент для списка специальностей
 */
public class SpecialtyItem extends AbstractItem<SpecialtyItem, SpecialtyItem.ViewHolder> {

  private Specialty specialty;

  public SpecialtyItem(Specialty specialty) {
    this.specialty = specialty;
  }

  public Specialty getSpecialty() {
    return specialty;
  }

  @NonNull
  @Override
  public ViewHolder getViewHolder(View v) {
    return new ViewHolder(v);
  }

  @Override
  public int getType() {
    return SPECIALTY_ITEM_ID;
  }

  @Override
  public int getLayoutRes() {
    return R.layout.specialty_item;
  }

  public class ViewHolder extends FastAdapter.ViewHolder<SpecialtyItem> {

    private TextView name;
    private CardView card;

    ViewHolder(View itemView) {
      super(itemView);
      name = itemView.findViewById(R.id.name);
      card = itemView.findViewById(R.id.card);
    }

    @Override
    public void bindView(SpecialtyItem item, List<Object> payloads) {
      name.setText(item.getSpecialty().getSpecialtyName());
      card.setCardBackgroundColor(
          ContextCompat.getColor(itemView.getContext(), R.color.colorAccent));
    }

    @Override
    public void unbindView(SpecialtyItem item) {
      name.setText(null);
    }

  }
}
