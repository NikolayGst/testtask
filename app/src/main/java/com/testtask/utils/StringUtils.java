package com.testtask.utils;


public class StringUtils {

  /**
   * метод возвращает окончание для множественного числа слова на основании числа и массива
   * окончаний
   *
   * @param iNumber Integer Число на основе которого нужно сформировать окончание
   * @param aEndings Array Массив слов или окончаний для чисел (1, 4, 5), например ['яблоко',
   * 'яблока', 'яблок']
   */
  public static String getNumEnding(int iNumber, String... aEndings) {
    String sEnding;
    int i;
    iNumber = iNumber % 100;
    if (iNumber >= 11 && iNumber <= 19) {
      sEnding = aEndings[2];
    } else {
      i = iNumber % 10;
      switch (i) {
        case 1:
          sEnding = aEndings[0];
          break;
        case 2:
        case 3:
        case 4:
          sEnding = aEndings[1];
          break;
        default:
          sEnding = aEndings[2];
      }
    }
    return sEnding;
  }

  /**
   * метод возвращает строку, у которой первая буква заглавная
   *
   * @param text текст, который нужно обработать
   */
  public static String upperFirstLetter(String text) {
    return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
  }

}
