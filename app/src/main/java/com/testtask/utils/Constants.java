package com.testtask.utils;

public class Constants {

  public static final String BASE_URL = "http://gitlab.65apps.com";

  public static final int SPECIALTY_ITEM_ID = 100;
  public static final int USER_ITEM_ID = 200;

  public static final String SPECIALTY_ID_KEY = "specialty_id";
  public static final String USER_ID_KEY = "user_id";

}
