package com.testtask.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

  /**
   * Метод возвращает кол-во прошедших дней со дня рождения
   */
  public static int getYearByBirthday(String date) {

    if (date == null) {
      return -1;
    }

    Date currentDate = new Date();

    //Два паттерна для парсинга даты
    String datePattern = date.matches("\\d{2}-\\d{2}-\\d{4}") ? "dd-MM-yyyy" : "yyyy-MM-dd";

    SimpleDateFormat format = new SimpleDateFormat(datePattern, Locale.getDefault());

    try {
      Date userDate = format.parse(date);

      long time = currentDate.getTime() - userDate.getTime();

      return (int) (time / 1000 / 60 / 60 / 24 / 365);

    } catch (ParseException e) {
      return -1;
    }
  }

  /**
   * Метод возвращает отформатированную дату для окна "Детали работника"
   */
  public static String formatDateForDetail(String date) {

    if (date == null) {
      return "-";
    }

    //Два паттерна для парсинга даты
    String datePattern = date.matches("\\d{2}-\\d{2}-\\d{4}") ? "dd-MM-yyyy" : "yyyy-MM-dd";

    SimpleDateFormat userDateFormat = new SimpleDateFormat(datePattern, Locale.getDefault());

    SimpleDateFormat detailFormat = new SimpleDateFormat("dd.MM.yyyy 'г.'", Locale.getDefault());

    try {

      Date userDate = userDateFormat.parse(date);
      int year = getYearByBirthday(date);

      return String.format(Locale.getDefault(), "%s (%d %s)",
          detailFormat.format(userDate),
          year,
          StringUtils.getNumEnding(year, "год", "года", "лет"));

    } catch (ParseException e) {
      return "-";
    }
  }

}
