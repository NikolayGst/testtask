package com.testtask.model;

import com.orm.SugarRecord;

public class Specialty extends SugarRecord {

  private int specialtyId;
  private String specialtyName;

  public Specialty() {

  }

  public Specialty(int specialtyId, String specialtyName) {
    this.specialtyId = specialtyId;
    this.specialtyName = specialtyName;
  }

  public int getSpecialtyId() {
    return specialtyId;
  }

  public String getSpecialtyName() {
    return specialtyName;
  }

}
