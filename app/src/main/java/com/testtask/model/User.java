package com.testtask.model;

import com.orm.SugarRecord;

public class User extends SugarRecord {

  private String firstName;

  private String LastName;

  private String birthday;

  private String avatarUrl;

  private int specialtyId;

  private String specialtyName;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return LastName;
  }

  public void setLastName(String lastName) {
    LastName = lastName;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public int getSpecialtyId() {
    return specialtyId;
  }

  public void setSpecialtyId(int specialtyId) {
    this.specialtyId = specialtyId;
  }

  public String getSpecialtyName() {
    return specialtyName;
  }

  public void setSpecialtyName(String specialtyName) {
    this.specialtyName = specialtyName;
  }
}

