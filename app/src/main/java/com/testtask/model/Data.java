package com.testtask.model;

import java.util.ArrayList;
import java.util.List;

public class Data {

  public Data() {}

  public Data(List<Specialty> specialties, List<User> users) {
    this.specialties = specialties;
    this.users = users;
  }

  private List<Specialty> specialties = new ArrayList<>();

  private List<User> users = new ArrayList<>();

  public List<Specialty> getSpecialties() {
    return specialties;
  }

  public void setSpecialties(List<Specialty> specialties) {
    this.specialties = specialties;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

}
